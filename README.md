# CoTech Newsletter

The CoTech Newsletter is a monthly post summarising all of the news and events from across [CoTech].

The work to produce this monthly newsletter is funded by the CoTech fund.

[cotech]: https://www.coops.tech/

# How To Subscribe

https://community.coops.tech/t/about-the-newsletter-category/626

# Past Editions

https://community.coops.tech/c/newsletter

# How We Do It

- We manually pass through sources.md and scope out what people are doing.
- We then build the markdown file in the editions folder.
- We release it on the last thursday of the month by making a new post on the CoTech Discourse.

# Time tracking

See [timetracking.md](timetracking.md).

# Invoicing

See [invoices](./invoices/README.md).
