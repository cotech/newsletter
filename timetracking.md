# Time tracking

## June 2021

- decentral1se: 2021-06-28: 2.30 hrs (also didn't have much time but ran through it)

## May 2021

- decentral1se: 2021-05-27: 3.39 hrs (flash fry the edition in one go)

## April 2021

- decentral1se: 2021-04-20: 1 hr (initial drafting)
- decentral1se: 2021-04-30: 2 hrs (finalising the edition)

## March 2021

- decentral1se: 2021-03-04: 1hr 30mins (initial scout for content)
- decentral1se: 2021-03-22: 3 hrs (first draft)
