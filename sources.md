# Sources

Here's a list of places that we all agree we should grab content from.

It's a manual slog right now. Maybe we'll improve that in the future.

Happy Hunting.

---

# Social Media

> https://www.coops.tech/#members

Note. Some cooperatives are still missing here from the above list.

## Agile.Coop

> https://twitter.com/AgileCollective

> https://agile.coop/blog

## Animorph

> https://animorph.coop/

## Aptivate

> https://twitter.com/aptivateuk

> http://aptivate.org/en/blog/

## Autonomic

> https://twitter.com/autonomiccoop

> https://autonomic.zone/blog/

## Blake House

> https://twitter.com/blake_house

## Calverts Coop

> https://twitter.com/calverts

> http://www.calverts.coop/news/

## Founders and Coders

> https://twitter.com/founderscoders

## Media Coop

> https://twitter.com/mediaco_op

> http://mediaco-op.net/category/blog

## Open Data Services

> https://twitter.com/opendatacoop

> http://www.opendataservices.coop/blog/

## Outlandish

> https://twitter.com/outlandish

> https://outlandish.com/blog/

## Small Axe

> https://twitter.com/thesmallaxecom

## Space4

> https://twitter.com/space4coop/

## We Are Open

> https://twitter.com/weareopencoop

## WebArchitects

> https://twitter.com/webarchcoop

> https://www.webarchitects.coop/

---

# Loomio

> https://www.loomio.org/g/oVwtKDOn/cotech

---

# Mailing List

> https://www.email-lists.org/mailman/listinfo/tech-coops

---

# Forum

> https://community.coops.tech

---

# Newsletter Issue Tracker

> https://git.coop/cotech/newsletter/issues

---

# Outside The Network

> https://www.facebook.com/groups/workercoops/

> https://twitter.com/nwspk
